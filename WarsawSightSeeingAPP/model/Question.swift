//
//  Question.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 11/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import Foundation

class Question {
    
    var content:String
    var xCords:Double
    var yCords:Double
    var information:String
    var quizNumber:Int
    init(content:String, xCords:Double,yCords:Double,information:String,quizNumber:Int){
        self.content = content
        self.xCords = xCords
        self.yCords = yCords
        self.information = information
        self.quizNumber = quizNumber
}

}
