//
//  ChooseRouteVC.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 11/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit

class ChooseRouteVC: UIViewController {

    
    @IBOutlet weak var mysteryTownButton: UIButton!
    
    @IBOutlet weak var gardenButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        // Do any additional setup after loading the view.
    }
    func configureView(){
        mysteryTownButton.layer.cornerRadius = 30
        mysteryTownButton.titleLabel?.numberOfLines = 2
        gardenButton.layer.cornerRadius = 30
        gardenButton.titleLabel?.numberOfLines = 2
    }

    @IBAction func onMysteryTownButtonClick(_ sender: Any) {
        LogicService.shared.setCurrentQuiz(route: .oldTown)
        
    }
    @IBAction func onGardenButtonClick(_ sender: Any) {
        LogicService.shared.setCurrentQuiz(route: .garden)

    }
   

}
