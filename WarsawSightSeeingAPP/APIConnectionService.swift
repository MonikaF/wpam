//
//  APIConnectionController.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 11/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit
import Alamofire
class APIConnectionService {

    
    var dataURL:String = "https://apiquestionlocalization.herokuapp.com/question/getAll"
    
    
    func getDataFromServer(){
        Alamofire.request(dataURL,method:.get,headers:nil).responseJSON{ (response:DataResponse) in
                switch(response.result){
                case .success(let value):
                    let data =  value as! [NSDictionary]
                    var qlist1 = [Question]()
                    var qlist2 = [Question]()
                    for d in data {
                        let q = d as! NSDictionary
                        var tmp  = q["content"] as! String
                        let tmpList = tmp.components(separatedBy: "|")
                        var qestion=Question(content: tmpList[0] ,
                                 xCords: q["xcordAnswer"] as! Double,
                                 yCords: q["ycordAnswer"] as! Double,
                                 information: q["city"] as! String,
                                 quizNumber: Int(tmpList[1])!)
                        if(Int(tmpList[1]) == 1){
                            qlist1.append(qestion)
                        }else {
                            qlist2.append(qestion)
                        }
                       

                    }
                    
                    var listOfQuizes = [Quiz]()
                    listOfQuizes.append(Quiz(questions:qlist1))
                    listOfQuizes.append(Quiz(questions:qlist2))
                    LogicService.shared.quizes = listOfQuizes
                case .failure(let value):
                    print("ERROR")
                }
        }
    }

        
    
    
    
    
    
    
    
    
    class var shared: APIConnectionService {
        struct Static {
            static let instance = APIConnectionService()
        }
        return Static.instance
    }
}
