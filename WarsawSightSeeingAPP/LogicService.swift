//
//  LogicController.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 11/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit
import LocalAuthentication
import CoreLocation

class LogicService{
    
    var score:Int = 0
    var currentRoute:Routes!
    var currentQuiz:Quiz!
    var actualQuestionNumber:Int!
    var quizes:[Quiz]!
    
    var locationManager:CLLocationManager!

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    func prepareData(){
  
        APIConnectionService.shared.getDataFromServer()
        
    }
    
    func setCurrentQuiz(route:Routes){
        if route == .oldTown {
            currentQuiz = quizes[0]
        }else{
            currentQuiz = quizes[1]
        }
        actualQuestionNumber = 0
        
    }
    
    func nextQuestion() -> Bool {
        self.score = self.score + 30

        self.actualQuestionNumber = self.actualQuestionNumber + 1
        if(actualQuestionNumber == currentQuiz.questions.count){
            self.actualQuestionNumber = 0
            return false
        }
        return true
    }
        

    
    
    
    class var shared: LogicService {
        struct Static {
            static let instance = LogicService()
        }
        return Static.instance
    }

    
    
    
    enum Routes {
        case oldTown
        case garden
        
    }
}
