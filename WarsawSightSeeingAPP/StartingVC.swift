//
//  StartingVC.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 30/04/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit

class StartingVC: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LogicService.shared.prepareData()
        configure()
        
        // Do any additional setup after loading the view.
    }
    
    func configure(){
        self.startButton.backgroundColor = UIColor.red
        self.startButton.layer.cornerRadius = self.startButton.frame.width/2
            self.scoreLabel.text = "\(LogicService.shared.score)"
    }
    


}
