//
//  QuestionVC.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 11/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit
import CoreLocation

class QuestionVC: UIViewController,CLLocationManagerDelegate {
    
    var locationManager:CLLocationManager!

    var question:Question!
    
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBOutlet weak var numberOfQuestionLabel: UILabel!
    
    
    @IBOutlet weak var contentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
    }
    
    func configure(){
        
        let qNumber = LogicService.shared.actualQuestionNumber!
        self.question = LogicService.shared.currentQuiz.questions[qNumber]
        numberOfQuestionLabel.text = "\(qNumber+1)/\(LogicService.shared.currentQuiz.questions.count)"
        contentLabel.text = LogicService.shared.currentQuiz.questions[LogicService.shared.actualQuestionNumber].content
        determineMyCurrentLocation()
        startChecking()
    
    }
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
 
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
    func startChecking(){

        DispatchQueue.global(qos: .background).async {
            var run = true
            var i  = 0
            while(run){
                sleep(3)
                let lon = self.locationManager.location?.coordinate.longitude
                let lat = self.locationManager.location?.coordinate.latitude
        
                
                var ansX = lon! - self.question.yCords
                ansX = ansX * ansX
                
                var ansY = lat! - self.question.xCords
                ansY = ansY*ansY
                var ans = sqrt(ansY+ansX)
                print(ans)
                if (ans < 0.002){
                    run = false
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "showInformationModal", sender: nil)
                        
                    }
                   
                    
                } else if (ans < 0.003){
                    DispatchQueue.main.async {
                        self.progressLabel.text = "Almost end of the jurney!!! "

                    }
                }
                else if (ans < 0.004){
                    DispatchQueue.main.async {
                        self.progressLabel.text = "You are closer!! "

                    }
                    
                }
                else if (ans > 0.004){
                    DispatchQueue.main.async {
                        self.progressLabel.text = "You are very very far!! "
                    }
                    
                    
                }
                
            }
           
        
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInformationModal" {
            let dest = segue.destination as! RewardController
            dest.question = question
            if (LogicService.shared.nextQuestion() == true){
                dest.isLastQuestion = false
            }
            else{
                dest.isLastQuestion = true
            }
        }
    }
}
