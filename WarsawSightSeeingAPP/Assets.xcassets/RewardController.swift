//
//  RewardController.swift
//  WarsawSightSeeingAPP
//
//  Created by Szczepan Gabiec on 12/05/2019.
//  Copyright © 2019 Monika. All rights reserved.
//

import UIKit

class RewardController: UIViewController {
    var question:Question!
    var isLastQuestion:Bool!
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var textButton: UIButton!
    
    @IBOutlet weak var swordButton: UIButton!
    
    @IBOutlet weak var finishJurneyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
        
        
    }
    
    func configuration(){
        content.text = question.information
        if(isLastQuestion){
            textButton.isEnabled = false
            textButton.isHidden = true
            
            swordButton.isHidden = true
            swordButton.isEnabled = false
            
            finishJurneyButton.isHidden = false
            finishJurneyButton.isEnabled = true
            
        }else{
            
            textButton.isEnabled = true
            textButton.isHidden = false
            
            swordButton.isHidden = false
            swordButton.isEnabled = true
            
            finishJurneyButton.isHidden = true
            finishJurneyButton.isEnabled = false
            
            
        }
    }
}
